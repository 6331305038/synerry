const Url = require("../models/urlModel");
const axios = require("axios");

const getAllData = async (req, res) => {
  try {
    const allData = await Url.find({});
    if (allData.length > 0) {
      res.json({ allData });
    } else {
      res.status(404).json({ error: "No data found" });
    }
  } catch (error) {
    console.error("Error in getAllData:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const shortenUrl = async (req, res) => {
  try {
    const longUrl = req.body.longUrl;
    const existingUrl = await Url.findOne({ longUrl });
    if (existingUrl) {
      res.json({
        message: "URL already exists",
        shortUrl: existingUrl.shortUrl,
      });
      return;
    }
    const response = await axios.get("https://is.gd/create.php", {
      params: {
        format: "json",
        url: longUrl,
      },
    });
    const shortUrl = response.data.shorturl;
    const url = new Url({
      longUrl,
      shortUrl,
    });
    await url.save();
    res.json({ shortUrl });
  } catch (error) {
    console.error("Error in shortenUrl:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const redirectToUrl = async (req, res) => {
  try {
    const shortUrl = req.params.shortUrl;
    const url = await Url.findOne({ shortUrl });

    if (url) {
      const longUrl = url.longUrl;
      await increaseClickCount(shortUrl);
      res.redirect(longUrl);
    } else {
      res.status(404).json({ error: "Short URL not found" });
    }
  } catch (error) {
    console.error("Error in redirectToUrl:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const getAllDataForLongUrl = async (req, res) => {
  try {
    const longUrl = decodeURIComponent(req.params.longUrl);
    const urlData = await Url.find({ longUrl: longUrl });

    if (urlData.length > 0) {
      res.json({ urlData });
    } else {
      res.status(404).json({ error: "No data found for the specified longUrl" });
    }
  } catch (error) {
    console.error("Error in getAllDataForLongUrl:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};


const increaseClickCount = async (shortUrl) => {
  try {
    const url = await Url.findOne({ shortUrl });

    if (url) {
      url.click += 1;
      await url.save();
    }
  } catch (error) {
    console.error("Error in increaseClickCount:", error);
  }
};

const deleteUrlbylongUrl = async (req, res) => {
  try {
    const longUrl = decodeURIComponent(req.params.longUrl);
    const url = await Url.findOneAndDelete({ longUrl });

    if (url) {
      res.json({ message: "URL deleted successfully" });
    } else {
      res.status(404).json({ error: "URL not found" });
    }
  } catch (error) {
    console.error("Error in deleteUrlbylongUrl:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = {
  getAllData,
  shortenUrl,
  redirectToUrl,
  getAllDataForLongUrl,
  increaseClickCount,
  deleteUrlbylongUrl,
};
