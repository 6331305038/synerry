const express = require('express');
const router = express.Router();
const urlController = require('../controllers/urlController');

router.get('/alldata', urlController.getAllData);
router.post('/shorten', urlController.shortenUrl);
router.get('/:shortUrl', urlController.redirectToUrl);
router.get('/data/:longUrl', urlController.getAllDataForLongUrl);
router.put('/click/:shortUrl', async (req, res) => {
    try {
      const shortUrl = decodeURIComponent(req.params.shortUrl);
      await urlController.increaseClickCount(shortUrl);
      res.status(200).json({ message: 'Click count increased successfully' });
    } catch (error) {
      console.error('Error in increaseClickCount route:', error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });
router.delete('/delete/:longUrl', urlController.deleteUrlbylongUrl);

module.exports = router;
