const mongoose = require("mongoose");

const urlSchema = new mongoose.Schema({
  longUrl: { type: String, required: true, unique: true },
  shortUrl: { type: String, required: true },
  click: { type: Number, default: 0 },
});

const Url = mongoose.model("Url", urlSchema);

module.exports = Url;
