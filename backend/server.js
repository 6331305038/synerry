const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const helmet = require("helmet");
const mongoose = require("mongoose");
require("dotenv").config();
const urlRoute = require("./routes/urlRoute");
const functions = require("firebase-functions");

const app = express();

// Connnect Database
mongoose
  .connect(process.env.DATABASE)
  .then(() => console.log("Connect mongodb success"))
  .catch((err) => console.log(err));

// Middleware
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));
app.use(helmet());

// Route
app.use("/api", urlRoute);

exports.api = functions.https.onRequest(app);
